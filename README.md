# Lucid Lore

Системa для проведения простого тестирования.

Включает в себя расширяемую админку с использованием компонентов Vuetify. Списки сущностей с пагинацией, фильтрацией, поиском и сортировкой. Базовые CRUD формы. Отдельные мутации. Немного realtime'а. Бэк на node.js. 

Основано на [Simple Blank Project](https://github.com/atam91/simple-blank-project) более подробные инструкции по запуску можно найти там.

## Project setup
Run `git submodule update --init --recursive`; Further instructions in submodules.
